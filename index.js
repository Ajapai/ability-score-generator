//HtmlElement
let _card = document.getElementsByClassName('flip-card-inner')[0];
let _scores = document.getElementsByClassName('score');
let _buttons = document.getElementsByClassName('button');
let _sliders = Object.fromEntries(["min", "max", "total"].map( x => [x, document.getElementById(x)]));
let _sliderOutputs = Object.fromEntries(["min", "max", "total"].map( x => [x, document.getElementById(x + "Out")]));
let _totalDropdown = document.getElementsByClassName("dropdown")[0];

// Event listeners
for (callback of [minOnChange, maxOnChange, totalOnChange]) {
  _sliders[callback.name.replace("OnChange", "")].oninput = callback;
}

// Value objects
const sliderValues = {
  get min() {
    return parseInt(_sliders.min.value);
  },
  set min(value) {
    _sliders.min.value = value;
  },
  get max() {
    return parseInt(_sliders.max.value);
  },
  set max(value) {
    _sliders.max.value = value;
  },
  get total() {
    return parseInt(_sliders.total.value);
  },
  set total(value) {
    _sliders.total.value = value;
  },
  get totalType() {
    return parseInt(_totalDropdown.value);
  },
  set totalType(value) {
    _totalDropdown.value = value;
  }
}

// Rules
function minOnChange() {
  _sliderOutputs.min.innerHTML = sliderValues.min;
  if (sliderValues.max < sliderValues.min) {
    sliderValues.max = _sliderOutputs.max.innerHTML = sliderValues.min;
  }
  if (sliderValues.total < sliderValues.min * 6) {
    sliderValues.total = sliderValues.min * 6;
    _sliderOutputs.total.innerHTML = _sliders.total.disabled ? "" : sliderValues.min * 6;
  }
}

function maxOnChange() {
  _sliderOutputs.max.innerHTML = sliderValues.max;
  if (sliderValues.min > sliderValues.max) {
    sliderValues.min = _sliderOutputs.min.innerHTML = sliderValues.max;
  }
  if (sliderValues.total > sliderValues.max * 6) {
    sliderValues.total = sliderValues.max * 6;
    _sliderOutputs.total.innerHTML = _sliders.total.disabled ? "" : sliderValues.max * 6;
  }
}

function totalOnChange() {
  _sliderOutputs.total.innerHTML = sliderValues.total;
  if (sliderValues.min * 6 > sliderValues.total) {
    sliderValues.min = _sliderOutputs.min.innerHTML = Math.floor(sliderValues.total / 6);
  }
  if (sliderValues.max * 6 < sliderValues.total) {
    sliderValues.max = _sliderOutputs.max.innerHTML = Math.ceil(sliderValues.total / 6);
  }
}

function toggleTotal() {
  if (_sliders.total.disabled) {
    _sliders.total.disabled = _totalDropdown.disabled = false;
    _sliderOutputs.total.innerHTML = _sliders.total.value;
    _buttons[2].value = "Disable";
  }
  else {
    _sliders.total.disabled = _totalDropdown.disabled = true;
    _sliderOutputs.total.innerHTML = "";
    _buttons[2].value = "Enable";
  }
}

function setDefaultRules() {
  for (let [key, value] of Object.entries({"min": 6, "max": 16, "total": 69})){
    _sliders[key].value = _sliderOutputs[key].innerHTML = value;
  }
  if (_sliders.total.disabled) {
    _sliders.total.disabled = _totalDropdown.disabled = false;
    _buttons[2].value = "Disable";
  }
  sliderValues.totalType = 0;
}

// Score generater
const abilityScores = {
  get values() {
    return Array.from(_scores, x => parseInt(x.innerHTML))
  },
  get allEqual() {
    let scores = this.values;
    return scores.every(x => x === scores[0])
  }
}

let scoreGenerators = [];
let currentMin = 0;
let currentMax = 0;
let currentDice = 0;
let currentTotal = 0;
const nextRange = {
  get min() { return (5 - currentDice) * currentMin + currentTotal + currentMax }, 
  get max() { return (5 - currentDice) * currentMax + currentTotal + currentMin }
}

function adjustCurrentMinMax() {
  if (_sliders.total.disabled) {
    return;
  }
  while (nextRange.max < sliderValues.total && sliderValues.totalType !== 2) {
    currentMin += 1;
  }
  while (nextRange.min > sliderValues.total && sliderValues.totalType !== 1) {
    currentMax -= 1;
  }
}

function randomNumber(min, max) {
  return Math.floor(Math.random() * (max - min + 1) ) + min;
}

function generateScore() {
  return randomNumber(currentMin, currentMax);
}

// Buttons
function start() {
  _buttons[0].value = "Reset";
  _buttons[0].onclick = reset;
  _buttons[1].value = "Stop";
  _buttons[1].onclick = stop;
  
  currentMin = sliderValues.min;
  currentMax = sliderValues.max;
  adjustCurrentMinMax();

  for (let i = 0; i < 6; i++) {
    _scores[i].innerHTML = generateScore();
    scoreGenerators[i] = setInterval(function (index) { _scores[index].innerHTML = generateScore() }, 50, i)
  }
  if (currentMin === currentMax || sliderValues.min === sliderValues.max) {
    _buttons[1].disabled = true;
    finish();
  }
}

function stop() {
  _buttons[1].disabled = true;
  clearInterval(scoreGenerators[currentDice]);
  currentTotal += parseInt(_scores[currentDice].innerHTML);
  currentDice += 1;
  adjustCurrentMinMax();
  if (currentMin === currentMax || currentDice === 6) {
    finish();
  }
  else {
    _buttons[1].disabled = false;
  }
}

function finish() {
  setTimeout( () => {
    for (let i = currentDice; i < 6; i++) {
      clearInterval(scoreGenerators[i]);
    }
  }, 100);

  if (!abilityScores.allEqual){
    _buttons[1].value = "Shuffle";
    _buttons[1].onclick = shuffle;
    _buttons[1].disabled = false;
  }
}

function shuffle() {
  _buttons[0].disabled = _buttons[1].disabled = true;
  let scores = abilityScores.values;
  scores = scores
    .map(x => ({x, s: Math.random()}))
    .sort((a, b) => a.s - b.s)
    .map(({x}) => x)
  for (const [index, score] of scores.entries()) {
    _scores[index].innerHTML = score;
  }
  repeateShuffle(scores, 50, randomNumber(150, 350));
}

function repeateShuffle(scores, timer, limit) {
  scores = scores
    .map(x => ({x, s: Math.random()}))
    .sort((a, b) => a.s - b.s)
    .map(({x}) => x)
  for (const [index, score] of scores.entries()) {
    _scores[index].innerHTML = score;
  }
  if (timer < limit) {
    setTimeout(repeateShuffle, timer, scores, timer + 10, limit)
  }
  else {
    setTimeout( () => {
      for (const [index, color] of ["crimson", "green", "darkorange", "mediumorchid", "deepskyblue", "deeppink"].entries()) {
        _scores[index].parentElement.style.borderColor = _scores[index].style.color = color;
      }
      _buttons[0].disabled = false;
    }, timer * 2)
  }
}

function reset() {
  _buttons[0].value = "Set Rules";
  _buttons[0].onclick = flipCard;
  _buttons[1].value = "Start";
  _buttons[1].onclick = start;
  _buttons[1].disabled = false;

  for (let i = 0; i < 6; i++) {
    clearInterval(scoreGenerators[i]);
    _scores[i].innerHTML = "";
    _scores[i].parentElement.style.borderColor = _scores[i].style.color = "inherit";
  }
  currentDice = 0;
  currentTotal = 0;
}

function flipCard() {
  _card.style.transform = _card.style.transform == "rotateY(180deg)" ?
                          _card.style.transform = "rotateY(0deg)" :
                          _card.style.transform = "rotateY(180deg)";
}

function toggleDarkMode(event) {
  let body = document.getElementsByTagName("body")[0];
  if (body.classList.contains("dark-mode")) {
    event.target.src = "dark-mode.png";
    body.classList.remove("dark-mode");
    _totalDropdown.classList.remove("dark-mode");
    document.cookie = "dark-mode=; ; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/ability-score-generator;";
  }
  else {
    event.target.src = "light-mode.png";
    body.classList.add("dark-mode");
    _totalDropdown.classList.add("dark-mode");
    document.cookie = "dark-mode=true; expires=Fri, 31 Dec 9999 23:59:59 GMT";
  }
}

if (document.cookie === "dark-mode=true") {
  document.getElementsByTagName("img")[0].src = "light-mode.png";
  document.getElementsByTagName("body")[0].classList.add("dark-mode");
  _totalDropdown.classList.add("dark-mode");
}